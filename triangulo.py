#!/usr/bin/env pythoh3

import sys
def line(number: int):
    return str(number) * number

def triangle(number: int):
    try:
        if number > 9:
            raise ValueError
        linea = ""
        for num_linea in range(1, number + 1):
            linea = linea + (line(num_linea)) + "\n"
        return linea
    except ValueError:
        return triangle(9)

def main():
    try:
        number: int = sys.argv[1]
        if int(number) > 9 or int(number) < 1:
            raise ValueError
        text = triangle(int(number))
        return text
    except ValueError:
        return "Please provide a number (between 1 and 9) as argument"
    except IndexError:
        return "Please provide a number (between 1 and 9) as argument"


if __name__ == '__main__':
    print(main())


